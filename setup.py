"""Setup file for moniker"""
from setuptools import setup, find_packages


TEST_REQUIREMENTS = ['pytest', 'pytest-cov', 'pylint', 'mypy', 'flake8']

with open('README.md') as f:
    README = f.read()

with open('LICENSE') as f:
    LICENSE = f.read()

with open('requirements.txt') as f:
    REQUIREMENTS = f.readlines()

setup(
    name='moniker',
    version='0.0.1',
    description='A script for tagging, organizing, and managing aliases and scripts.',
    long_description=README,
    author='Eugene Kovalev',
    author_email='eugene@kovalev.systems',
    url='https://gitlab.com/abraxos/moniker',
    license=LICENSE,
    packages=find_packages(exclude=('tests', 'docs')),
    entry_points={'console_scripts': ['moniker=moniker:cli']},
    install_requires=REQUIREMENTS + TEST_REQUIREMENTS,
)
