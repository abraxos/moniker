# moniker

Inspired by [pier](https://github.com/BenSchZA/pier), moniker is a way to keep track of aliases in an organized manner.

## Why?

I really liked [pier](https://github.com/BenSchZA/pier), but it just didn't work for me because of how it needed to run the scripts and stuff. I figured I could make a fairly simply script with very similar functionality that could work with aliases-files and comments within those files. It keeps things like descriptions and tags, but does not change how the aliases are run.

## Features

- [x] Scan the ~/.aliases file, interpret the aliases and comments
  - [x] CLI for moniker
- [x] Tags for aliases
- [ ] Configurable aliases files beyond ~/.aliases
- [ ] Adding custom scripts that are not aliases to moniker as well
- [ ] Editing aliases through moniker, like setting tags and descriptions

## Setup

Clone the repository and then install with:

```
pip install /path/to/repository
```

Then you can use `moniker --help` for more information on how to use it. As of right now, `moniker` only uses your `~/.aliases` file, but there are plans to add a configurable set of aliases files as well as custom scripts.

## Formatting the Aliases File

`moniker` fundementally recognizes two kinds of alias/comment formats, the one where the comment is directly above the alias, and one where the comment is on the same line as the alias. If both are detected, the latter will supercede the former and the comment above the alias will be ignored.

**On the same line**

```
alias emacs="emacs -nw" # Always launch emacs in a terminal
```

will be registered with `moniker` and if you execute `moniker list`, you will see something like:

```
+-----------------------------+-----------------------------+--------------------------+-----------------------------+
|            alias            |           actual            |           tags           |         description         |
+=============================+=============================+==========================+=============================+
| emacs                       | emacs -nw                   | []                       | Always launch emacs in      |
|                             |                             |                          | terminal                    |
+-----------------------------+-----------------------------+--------------------------+-----------------------------+
| hal                         | ls -hal                     | ['files', 'ls', 'basic'] | List directory with all     |
|                             |                             |                          | contents in human readable  |
|                             |                             |                          | format                      |
+-----------------------------+-----------------------------+--------------------------+-----------------------------+
| syu                         | sudo apt-get update --fix-  | []                       | Update Aliases              |
|                             | missing; sudo apt-get       |                          |                             |
|                             | upgrade -y; sudo apt-get    |                          |                             |
|                             | dist-upgrade -y; sudo apt-  |                          |                             |
|                             | get autoremove -y;          |                          |                             |
+-----------------------------+-----------------------------+--------------------------+-----------------------------+
```

**Comment immediately above alias**

```
# Always launch emacs in a terminal
alias emacs="emacs -nw"
```

**Tagging**

You can also attach tags to your aliases to make them easier to find. Tags must be in the format: `tags=[tag1,tag2,tag3]`

```
alias emacs="emacs -nw" # Always launch emacs in a terminal tags=[emacs,text-editor,text]
alias vim="vim" # Just launch vim tags=[vim,text-editor,text]
alias hal="ls -hal" # List files with nice formatting tags=[files]
```

This will then allow you to search for aliases tagged with a given tag, like so: `moniker list --tag emacs --tag vim`

```
+-------+-----------+----------------------------------+------------------------------------+
| alias |  actual   |               tags               |            description             |
+=======+===========+==================================+====================================+
| emacs | emacs -nw | ['emacs', 'text-editor', 'text'] | Always launch emacs in a terminal  |
+-------+-----------+----------------------------------+------------------------------------+
| vim   | vim       | ['text', 'text-editor', 'vim']   | Just launch vim                    |
+-------+-----------+----------------------------------+------------------------------------+s
```