from typing import Optional, Set, List
from pathlib import Path
from shutil import get_terminal_size
import re

import click
from texttable import Texttable


# https://regex101.com/r/NKFcr7/5
ALIAS_PAT = re.compile(r"((^alias\s+(?P<alias_a>\S+) *=(\'|\")(?P<aliased_a>.*)(\'|\") *\;? *\#+ *(?P<comment_a>[^\[\n]*)(tags ?= ?\[(?P<tags_a>.+).*\])?$)|((^\#+ *(?P<comment_b>[^\[\n]*)(tags ?= ?\[(?P<tags_b>.+).*\])? *$\n)?^alias +(?P<alias_b>\S+)=(\'|\")(?P<aliased_b>.+)(\'|\") *;? *$))", flags=re.M)


class Alias():
    @staticmethod
    def from_match(match) -> Optional['Alias']:
        D = match.groupdict()
        if any(D[k] is not None for k in D.keys() if k.endswith('_a')):
            return Alias(D['alias_a'], D['aliased_a'],
                         D['tags_a'], D['comment_a'])
        elif any(D[k] is not None for k in D.keys() if k.endswith('_b')):
            return Alias(D['alias_b'], D['aliased_b'],
                         D['tags_b'], D['comment_b'])
        return None

    def __init__(self, alias: str, aliased: str,
                 tags: Optional[str], description: Optional[str]) -> None:
        self.alias: str = alias
        self.aliased: str = aliased
        self.tags: Set[str] = set(t.strip() for t in tags.split(',')) if tags is not None else set()
        self.description: str = description if description is not None else ''

    def row(self) -> List[str]:
        return [self.alias, self.aliased,
                str(list(self.tags)), self.description]

    def is_tagged(self, tags: List[str]) -> bool:
        return any(tag in self.tags for tag in tags)


@click.group()
@click.option('--aliases-file', default=str(Path.home() / '.aliases'),
              type=click.Path(exists=True, file_okay=True, dir_okay=False,
                              writable=False, readable=True, resolve_path=True),
              help='Set the aliases file to use for moniker, defaults to {}'.format(str(Path.cwd() / '.aliases')))
@click.pass_context
def cli(ctx: click.Context, aliases_file: str) -> None:
    ctx.ensure_object(dict)
    ctx.obj['ALIASES_FILE'] = str(aliases_file)


@cli.command('list')
@click.pass_context
@click.option('--tag', '-t', multiple=True, help='One (or multiple) tags for aliases to be listed')
def list_aliases(ctx: click.Context, tag: List[str]) -> None:
    contents = Path(ctx.obj['ALIASES_FILE']).open().read()
    table = Texttable()
    table.set_max_width(get_terminal_size((80, 20)).columns)
    table.header(["alias", "actual", "tags", "description"])
    table.set_cols_align(["l", "l", "l", "l"])
    aliases = list(Alias.from_match(m) for m in ALIAS_PAT.finditer(contents))
    table.add_rows((a.row() for a in aliases if a is not None and (not tag or a.is_tagged(tag))), header=False)
    print(table.draw())


if __name__ == '__main__':
    cli()  # pylint: disable=E1120
